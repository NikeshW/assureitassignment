## The purpose of this repository

The purpose of this repository is to provide a solution to test the below acceptance criteria.

When "https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false" request is made, validate the below acceptance criteria
in the response.

1) Name = "Carbon credits"
2) CanRelist = true
3) The Promotions element with Name = "Gallery" has a Description that contains the text "2x larger image"

## Pre-Conditions:
1) Java should be installed in the machine and system variables should be configured
2) eclipse or IntelliJ IDEA should be available
3) Cucumber should be added as a plugin to one of the above two IDEs
4) Maven should be added into the IDE as a plugin

## Technology used:
1) Java (jdk_1.8)
2) Maven (Dependency management)
3) TestNG (Reporting/Execution)
4) Cucumber (BDD Plugin)
5) Extent reporting (Reporting)
6) Json data file (Data provider)

## How to import the project:

1) Use the given(this) "https://NikeshW@bitbucket.org/NikeshW/assureitassignment.git" repository and 
   clone the repository into a local folder (use the default terminal for the below points)
   "Navigate to the desire folder and get Bit bash here"
   "gin init"
   "git clone https://NikeshW@bitbucket.org/NikeshW/assureitassignment.git"
2) As per eclipse --->> go to file -> import -> Existing projects into workspace -> Select root directory -> browse the cloned 
   repository and click finish
3) Observe the imported project is appeared under Project Explorer

## Package Structure:
1) Common - common methods can be used in validations
2) ExtentReport - contains the ExtentReport class helps in defining the extent reporting methods
3) features - Cucumber feature file used in behavioral driven development
4) ReusableRequest - Get request method can be used as a common request in all the requests
5) Runner - class which integrates the feature file and the stepdefinition and TestNG
6) StepDefinition - Java Implementation of the feature file
7) Utility - JsonReader class uses for reading the request url
8) Data folder - Json file provides the request url 


## How to execute the project:

** Using the feature file (CategoryDetails.feature)
1) Right click the feature file and select Run As -> Cucumber Feature

** Using the TestRunner class:
1) Right click the TestRunner class and select Run As -> TestNG Test

## How to verify test results:
1) Console will provide the default console output
2) Navigate to the project folder -> test-output -> ExecutionReport.html and open in a web browser (Chrome)
