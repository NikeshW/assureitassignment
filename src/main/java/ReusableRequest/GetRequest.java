package ReusableRequest;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetRequest {

	RequestSpecification httpsRequest = RestAssured.given();

	public JsonPath getRequest_ResponseToJson(String url) {
		// Making the GET request and return the response as a Json and convert it to a
		// JsonPath
		Response response = httpsRequest.get(url);
		JsonPath jsonPathEvaluator = response.getBody().jsonPath();

		return jsonPathEvaluator;
	}
}
