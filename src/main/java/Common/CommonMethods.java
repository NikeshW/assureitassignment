package Common;

import java.util.List;
import java.util.Map;
import io.restassured.path.json.JsonPath;

public class CommonMethods {

	public String getValuefromJsonResponse(JsonPath Jsonresponse, String attribute) {

		String parameter = Jsonresponse.get(attribute);

		return parameter;
	}

	public Boolean getBooleanValuefromResponse(JsonPath Jsonresponse, String attribute) {

		Boolean parameter = Jsonresponse.get(attribute);

		return parameter;
	}

	public boolean getResponseAttribute(JsonPath Jsonresponse, String NameKey, String Element, String name,
			String elementProperty, String elementPropertyValue) {

		List<Map<String, String>> JsonNode = Jsonresponse.getList(Element);

		for (int i = 0; i < JsonNode.size(); i++) {
			if (JsonNode.get(i).get(NameKey).equals(name)) {
				if (JsonNode.get(i).get(elementProperty).contains(elementPropertyValue)) {

					return true;
				}

			}

		}

		return false;
	}
}
