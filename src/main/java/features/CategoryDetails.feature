Feature: Verify the response body of the getCategoryDetails service

  Scenario: Verify the category details
    Given I have a URL to get category details
    When I successfully send the request
    Then Verify the Category Name is "Carbon credits" in the received response
    And Verify the CanRelist of the category is "true" in the received response
    And The element "Promotions" has a Name "Gallery"  with "Description" contains "2x larger image" in the received response
