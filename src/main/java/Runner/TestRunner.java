package Runner;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(
	
		glue = {"StepDefinition"},//Step definitions package.
		features = {"src/main/java/features"})

public class TestRunner extends AbstractTestNGCucumberTests {

}